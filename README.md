# Selenium Controls #

Selenium controls is a Java library that models the standard HTML controls, providing interaction methods via Selenium WebDriver. The controls can then be used as part of automated web UI tests.

## Purpose ##

Often when creating automated web UI tests, the automation engineers create a POM (page object model), with each page class providing methods to interact with the controls on that page. This often leads to repitition of code within the page object classes, as no seperate *Control* layer is built as part of the application model.

With Selenium Controls the page-object-model can be adjusted to become the control-page-object-model.